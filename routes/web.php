<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/auth/{provider}/redirect',[\App\Http\Controllers\Auth\ProviderController::class,'redirect']);
Route::get('/auth/{provider}/callback',[\App\Http\Controllers\Auth\ProviderController::class,'callback']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    // categories route
    Route::prefix('categories')->group(function () {
        Route::get('/', [\App\Http\Controllers\CategoryController::class, 'index'])->name('categories.index');
        Route::get('/create', [\App\Http\Controllers\CategoryController::class, 'create'])->name('categories.create');
        Route::post('/', [\App\Http\Controllers\CategoryController::class, 'store'])->name('categories.store');
        Route::get('/{category}', [\App\Http\Controllers\CategoryController::class, 'show'])->name('categories.show');
        Route::get('/{category}/edit', [\App\Http\Controllers\CategoryController::class, 'edit'])->name('categories.edit');
        Route::put('/{category}', [\App\Http\Controllers\CategoryController::class, 'update'])->name('categories.update');
        Route::delete('/{category}', [\App\Http\Controllers\CategoryController::class, 'destroy'])->name('categories.destroy');
    });

    // post route
    Route::prefix('posts')->group(function () {
        Route::get('/', [\App\Http\Controllers\PostController::class, 'index'])->name('post.index');
        Route::get('/create', [\App\Http\Controllers\PostController::class, 'create'])->name('post.create');
        Route::post('/', [\App\Http\Controllers\PostController::class, 'store'])->name('post.store');
        Route::get('/{post}', [\App\Http\Controllers\PostController::class, 'show'])->name('post.show');
        Route::put('/{post}', [\App\Http\Controllers\PostController::class, 'update'])->name('post.update');
        Route::delete('/{post}', [\App\Http\Controllers\PostController::class, 'destroy'])->name('post.destroy');
        Route::get('/{post}/edit', [\App\Http\Controllers\PostController::class, 'edit'])->name('post.edit');
    });

    // comments route
    Route::post('/comments', [\App\Http\Controllers\CommentController::class, 'store'])->name('comments.store');

});

// add any route here if you want to it accessible for only admin
Route::group(['middleware' => ['role:admin']], function () {

    Route::delete('/comments/{comment}', [\App\Http\Controllers\CommentController::class, 'destroy'])->name('comment.destroy');

});
require __DIR__.'/auth.php';
