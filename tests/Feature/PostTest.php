<?php

namespace Tests\Feature;
use Tests\TestCase;



class PostTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index_route_returns_200_status()
    {
        $response = $this->get('/posts');

        $response->assertStatus(302); // Follow redirect
        $response->assertRedirect('/login'); // Assuming it redirects to login
    }



    public function test_create_route_returns_200_status()
    {
        $response = $this->get('/posts/create');

        $response->assertStatus(302); // Follow redirect
        $response->assertRedirect('/login'); // Assuming it redirects to login
    }



}
