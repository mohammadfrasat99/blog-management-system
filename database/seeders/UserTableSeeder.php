<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create admin role if not exists
        $adminRole = Role::where('name', 'admin')->first();
        if (!$adminRole) {
            $adminRole = Role::create(['name' => 'admin']);
        }

        // Create user role if not exists
        $userRole = Role::where('name', 'user')->first();
        if (!$userRole) {
            $userRole = Role::create(['name' => 'user']);
        }

        // Create admin user
        $admin = User::updateOrCreate(
            ['email' => 'admin@example.com'], // Admin user email
            [
                'name' => 'Admin User', // Admin user name
                'password' => bcrypt('password'), // Admin user password
            ]
        );

        // Assign admin role to admin user
        $admin->assignRole($adminRole);

        // Create user user
        $user = User::updateOrCreate(
            ['email' => 'user@example.com'], // User email
            [
                'name' => 'Regular User', // User name
                'password' => bcrypt('password'), // User password
            ]
        );

        // Assign user role to user
        $user->assignRole($userRole);
    }
}
