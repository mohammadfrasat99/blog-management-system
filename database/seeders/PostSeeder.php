<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 10 posts
        $posts = Post::factory()->count(10)->create();

        // Create comments for each post
        foreach ($posts as $post) {
            Comment::factory()->count(rand(1, 5))->create([
                'post_id' => $post->id,
            ]);
        }
    }
}
