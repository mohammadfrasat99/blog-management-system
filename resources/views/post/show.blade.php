<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Blog Post Details') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                @if(session('success'))
                    <div class="bg-green-100 border-l-4 border-green-500 text-green-700 p-4" role="alert">
                        <p class="font-bold">Success</p>
                        <p>{{ session('success') }}</p>
                    </div>
                @endif
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                    <div>
                        <h2 class="text-2xl  mb-4"><span class="font-bold">Post Title:</span>  <br> {{ $post->title }}</h2>
                        <p> <span class="font-bold">Category:</span>   <br> {{ $post->category->name }}</p>
                        <p><span class="font-bold">Created By:</span>  <br> {{ $post->user->name }}</p>
                        <p class="mt-4"><span class="font-bold">Content:</span><br> {!! $post->content !!}</p>
                    </div>

                    @if ($post->comments->isNotEmpty())
                        <div class="mt-6">
                            <h3 class="text-lg font-bold mb-2">Comments:</h3>
                            <div id="commentsContainer">
                            <ul id="commentsList">
                                <ol class="comment-list">
                                    @foreach ($post->comments as $comment)
                                        <li data-comment-id="{{ $comment->id }}">
                                            <div class="comment-content  rounded-md">
                                                {{ $comment->content }}
                                                <div class="comment-meta mt-2">
                                                    <p class="comment-author text-gray-400">Commented by: {{ $comment->user->name }}</p>
                                                    <p class="comment-date text-gray-400">Created at: {{ $comment->created_at->format('M d, Y H:i:s') }}</p>
                                                </div>
                                            </div>
                                            <form action="{{ route('comment.destroy', $comment->id) }}" method="POST" class="delete-comment-form">
                                                @csrf
                                                @method('DELETE')
                                                @if(auth()->check() && auth()->user()->hasRole('admin'))
                                                <button type="submit" class="deleteComments mb-4 text-red-600 hover:text-red-900" onclick="return confirm('Are you sure you want to delete this comment?')">
                                                    <i class="fas fa-trash"></i> Delete
                                                </button>
                                                @endif
                                            </form>
                                        </li>
                                    @endforeach
                                </ol>
                            </ul>
                            </div>
                        </div>
                    @endif

                    <div class="mt-8">
                        <h3 class="text-lg font-semibold mb-4">Add a Comment</h3>
                        <div class="mb-4">
                            <label for="content" class="block text-sm font-medium text-gray-700">Comment:</label>
                            <textarea id="content" name="content" rows="3" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
                        </div>
                        <div class="mt-4">
                            <button id="submitComment" data-post-id="{{ $post->id }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
        $(document).ready(function() {
            $('#submitComment').click(function(e) {
                e.preventDefault();
                var postId = $(this).data('post-id');
                var content = $('#content').val();
                $.ajax({
                    type: 'POST',
                    url: '{{ route('comments.store') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        post_id: postId,
                        content: content
                    },
                    success: function(response) {
                        if (response.status === 'success') {
                            $('#content').val(''); // Clear the comment textarea
                            // Check if the comments container exists
                            if ($('#commentsContainer').length === 0) {
                                // If the comments container doesn't exist, create it
                                $('#commentsList').parent().append('<div id="commentsContainer"><ul id="commentsList"></ul></div>');
                            }
                            // Append the new comment to the comments list
                            $('#commentsList').append('<li data-comment-id="' + response.comment.id + '">' +
                                '<p>' + response.comment.content + '</p>' +
                                '<p class="text-sm text-gray-400">Commented by: ' + response.comment.user.name + '</p>' +
                                '<p class="comment-date text-gray-400">Created at: ' + response.comment.created_at + '</p>' +
                                '<button type="button" class="deleteComments text-red-600 hover:text-red-900">' +
                                '<i class="fas fa-trash"></i> Delete</button>' +
                                '</li>');
                            alert('Comment added successfully');
                        } else {
                            alert('Failed to add comment');
                        }
                    },
                    error: function(xhr, status, error) {
                        alert('Error: ' + error);
                    }
                });
            });
        });


</script>
