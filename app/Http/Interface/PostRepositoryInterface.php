<?php
namespace App\Http\Interface;

use App\Models\Post;
use Illuminate\Http\Request;

interface PostRepositoryInterface
{
    public function all();

    public function create(Request $request);

    public function find($id);

    public function update(Request $request, Post $blogPost);

    public function delete(Post $blogPost);
}
