<?php
namespace App\Http\Repositories;


use App\Http\Interface\PostRepositoryInterface;
use App\Models\Post;
use Illuminate\Http\Request;
use League\CommonMark\CommonMarkConverter;

class PostRepository implements PostRepositoryInterface
{
    public function all()
    {
        return Post::orderBy('created_at', 'desc')->paginate(5);
    }

    public function create(Request $request)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|max:255',
            'content' => 'required|string',
        ]);

        $post = new Post();
        $post->user_id = auth()->user()->id;
        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->save();
        return $post;
    }

    public function find($id)
    {
        return Post::findOrFail($id);
    }

    public function update(Request $request, Post $post)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|max:255',
            'content' => 'nullable',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $converter = new CommonMarkConverter();
        $htmlContent = $converter->convertToHtml($request->content);

        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->content = $htmlContent;
        $post->save();

        return $post;
    }

    public function delete(Post $post)
    {
        $post->delete();
    }
}
