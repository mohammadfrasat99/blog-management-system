<?php
// app/Http/Controllers/PostController.php

namespace App\Http\Controllers;


use App\Http\Repositories\PostRepository;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index(Request $request)
    {
        $categories = Category::all();
        $query = Post::query();
        if ($request->has('category_id')) {
            $category_id = $request->input('category_id');
            $query->where('category_id', $category_id);
        }
        $posts = $query->latest()->paginate(5);
        return view('post.index', compact('categories','posts' ));
    }


    public function create()
    {
        $categories = Category::all();
        return view('post.create')->with(['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $blogPost = $this->postRepository->create($request);
        return redirect()->route('post.index')->with('success', 'Blog post created successfully.');
    }

    public function edit($id)
    {
        $categories = Category::all();
        $post = $this->postRepository->find($id);
        return view('post.edit', compact('post' ,'categories'));
    }



    public function show(Post $post)
    {
        $post->load('comments');
        return view('post.show', compact('post'));
    }


    public function update(Request $request, $id)
    {
        $post = $this->postRepository->find($id);
        $post = $this->postRepository->update($request, $post);
        return redirect()->route('post.index')->with('success', 'Blog post updated successfully.');
    }

    public function destroy($id)
    {
        $post = $this->postRepository->find($id);
        $this->postRepository->delete($post);
        return redirect()->route('post.index')->with('success', 'Blog post deleted successfully.');
    }
}
